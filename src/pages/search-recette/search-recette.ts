import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SearchRecetteDetailPage } from '../search-recette-detail/search-recette-detail';


@IonicPage()
@Component({
  selector: 'page-search-recette',
  templateUrl: 'search-recette.html',
})
export class SearchRecettePage {
    private title:string;

    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad SearchRecettePage');
    }

    go_back(){
        this.navCtrl.pop();
    }

    details(){
        this.navCtrl.push(SearchRecetteDetailPage, this.title);
    }





}
