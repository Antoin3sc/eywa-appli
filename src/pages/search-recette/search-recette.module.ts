import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchRecettePage } from './search-recette';

@NgModule({
  declarations: [
    SearchRecettePage,
  ],
  imports: [
    IonicPageModule.forChild(SearchRecettePage),
  ],
})
export class SearchRecettePageModule {}
