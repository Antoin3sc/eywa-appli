import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController } from 'ionic-angular';
import { TutoPage } from '../tuto/tuto';
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';

import { ApiProvider } from "../../providers/api/api";
import { SqliteProvider } from "../../providers/sqlite/sqlite";
import { BackgroundmodeProvider } from "../../providers/backgroundmode/backgroundmode";
import { BackgroundMode } from '@ionic-native/background-mode';

const DATABASE_FILE_NAME: string = 'data.db';

@IonicPage()
@Component({
    selector: 'page-welcome',
    templateUrl: 'welcome.html',
})
export class WelcomePage {

    private db: SQLiteObject;
    login: string;
    password: string;
    clicked: boolean = true;
    logo: boolean = true;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private sqlite: SQLite,

                private api: ApiProvider,
                private dbb: SqliteProvider,
                private bgModeProvider: BackgroundmodeProvider,
                private bgMode: BackgroundMode,
                public loadingCtrl: LoadingController,
                private platform: Platform
    ) {
        this.dbb.dbbSqlite('init', []   );
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad WelcomePage');
        this.platform.ready().then(() => {
        });
    }

    connexion() {
        this.checkAuth();
        console.log('Init DBB');
        this.navigate();
        this.clicked = !this.clicked;
        this.logo = true;
    }

    public checkAuth()
    {
        if (this.login && this.password) {
        this.api.getAuthWhereLogin(this.login, this.password)
            .subscribe(
                (response) => {
                    console.log(JSON.stringify(response));
                        if(response.length > 0){
                            this.checkDataBase();
                        }
                    },
                (error) => {
                    console.log('error');
                    console.log(JSON.stringify(error))
                }
            );
        }
    }

    public checkDataBase() {
        console.log('check if data user is empty');
        this.sqlite.create({
            name: DATABASE_FILE_NAME,
            location: 'default'
        })
            .then((db: SQLiteObject) => {
                console.log('bdd créée');
                this.db = db;
                this.db.executeSql("SELECT `login`, `mdp` FROM `User`", {})
                    .then((data) => {
                        let res = [];
                        for (let i = 0; i < data.rows.length; i++) {
                            res.push(data.rows.item(i));
                        }
                        if (res.some(e => e.login !== this.login)) {
                            this.dbb.dbbSqlite('insertUser', [this.login, this.password]);
                        }
                        this.clicked = !this.clicked;
                        this.navigate();
                        },
                        (e) => console.log(JSON.stringify(e)));
            });
    }


    navigate(){
        let loader = this.loadingCtrl.create({
            spinner: "hide",
            cssClass: 'my-loading-class',
            duration: 1000
        });

        loader.present();

        setTimeout(() => {
            this.navCtrl.push(TutoPage);
        }, 1000);
    }

    logoOff(){
        this.logo = !this.logo;
    }

}


