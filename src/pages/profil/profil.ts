import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { ApiProvider } from "../../providers/api/api";

import {SetPage} from "../set/set";
import {CaptorsPage} from "../captors/captors";

@IonicPage()
@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html',
})
export class ProfilPage {

  userData: any;
  id: number = 2;

  constructor(public navCtrl: NavController, public navParams: NavParams, private api: ApiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilPage');

      this.api.getUserInformations(this.id).subscribe(
          data => {
              this.userData = data;
              console.log("data"+ JSON.stringify(data));
          },
          error => {
              console.log("pas de data" + JSON.stringify(error))
          }
      );
  }





  // defi_page(){
  //     this.navCtrl.push(DefiPage);
  // }

  capteurs_page(){
      this.navCtrl.push(CaptorsPage);
  }

    setting_page(){
        this.navCtrl.push(SetPage);
    }

}
