import {Component, NgZone} from '@angular/core';
import {NavController, NavParams} from 'ionic-angular';
import {Platform} from 'ionic-angular';
import {BLE} from '@ionic-native/ble';

@Component({
    selector: 'page-detail',
    templateUrl: 'detail.html',
})
export class DetailPage {

    peripheral: any = {};
    statusMessage: string;
    characW: string;
    characR: string;
    service: string;
    data: any = [];


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                private ble: BLE,
                private ngZone: NgZone,
                public plt: Platform) {

        let device = navParams.get('device');

        this.setStatus('Connecting to ' + device.name || device.id);

        this.ble.connect(device.id).subscribe(
            peripheral => this.onConnected(peripheral),
            peripheral => this.onDeviceDisconnected(peripheral)
        );

        if (this.plt.is('ios')) {
            this.service = '00001204-0000-1000-8000-00805F9B34FB';
            this.characW = '00001A00-0000-1000-8000-00805F9B34FB';
            this.characR = '00001A01-0000-1000-8000-00805F9B34FB';
        } else if (this.plt.is('android')){
            this.service = '1204';
            this.characW = '1A00';
            this.characR = '1A01';
        }

    }

    onConnected(peripheral) {
        this.ngZone.run(() => {
            this.setStatus('');
            this.peripheral = peripheral;
        });
            let edit = new Uint8Array([0xA0, 0x1F]).buffer;

            this.ble.write(peripheral.id, this.service, this.characW, edit).then(
                function (res) {
                    console.log('yay')
                },
                function (error) {
                    alert('error : ' + error);
                }
            );

            this.ble.read(peripheral.id, this.service, this.characR).then(
                buffer => {
                    let tempdata = new Uint8Array(buffer);
                    let data = [];

                    //Temperature
                    data.push((tempdata[1] * 256 + tempdata[0]) / 10);
                    //Humidité
                    data.push(tempdata[7]);
                    //Lumière
                    data.push(tempdata[4] * 256 + tempdata[3]);
                    //Conductivité
                    data.push(tempdata[9] * 256 + tempdata[8]);

                    this.ngZone.run(() => {
                        this.data = data;
                    });


                }
            );



    }

    onSendingData(data){
        alert(data);
        this.ngZone.run(() => {
            this.statusMessage = data;
        });
    }

    onDeviceDisconnected(peripheral) {
    }

    // Disconnect peripheral when leaving the page
    ionViewWillLeave() {
        console.log('ionViewWillLeave disconnecting Bluetooth');
        this.ble.disconnect(this.peripheral.id).then(
            () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
            () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
        )
    }

    setStatus(message) {
        console.log(message);
        this.ngZone.run(() => {
            this.statusMessage = message;
        });
    }

}
