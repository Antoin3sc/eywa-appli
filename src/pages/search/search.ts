import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SearchPlantePage } from '../search-plante/search-plante';
import { SearchRecettePage } from '../search-recette/search-recette';
import { Platform } from 'ionic-angular';

import {ProfilPage} from "../profil/profil";

@IonicPage()
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {
  constructor(public navCtrl: NavController, public navParams: NavParams,platform: Platform) {
  
  	platform.registerBackButtonAction(() => {
  		console.log("backPressed 1");
  	}, 1);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPage');
  }

recherche_plante() {
	this.navCtrl.push(SearchPlantePage);
}

recherche_recette() {
	this.navCtrl.push(SearchRecettePage);
}

profil(){
      this.navCtrl.push(ProfilPage);
}

}