import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AidesPage } from './aides';

@NgModule({
  declarations: [
    AidesPage,
  ],
  imports: [
    IonicPageModule.forChild(AidesPage),
  ],
})
export class AidesPageModule {}
