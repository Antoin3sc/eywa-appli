import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCaptorPage } from './add-captor';

@NgModule({
  declarations: [
    AddCaptorPage,
  ],
  imports: [
    IonicPageModule.forChild(AddCaptorPage),
  ],
})
export class AddCaptorPageModule {}
