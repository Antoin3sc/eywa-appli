import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { CaptorPage } from "../captor/captor";
import { CaptorProvider } from "../../providers/captor/captor";
import {TabsPage} from "../tabs/tabs";

@IonicPage()
@Component({
    selector: 'page-add-captor',
    templateUrl: 'add-captor.html',
})


export class AddCaptorPage {

    peripheral: any = {};
    characW: string;
    characR: string;
    service: string;
    data: any = [];
    devices: any[] = [];

    constructor(public navCtrl: NavController,
                public plt: Platform,
                public navParams: NavParams,
                private captor: CaptorProvider) {

        if (this.plt.is('ios')) {
            this.service = '00001204-0000-1000-8000-00805F9B34FB';
            this.characW = '00001A00-0000-1000-8000-00805F9B34FB';
            this.characR = '00001A01-0000-1000-8000-00805F9B34FB';
        } else if (this.plt.is('android')){
            this.service = '1204';
            this.characW = '1A00';
            this.characR = '1A01';
        }
    }

    static ionViewDidLoad() {
        console.log('ionViewDidLoad AddCaptorPage');
    }

    go_back () {
        this.navCtrl.remove(0);        //this.navCtrl.push(CaptorPage);
        this.navCtrl.push(TabsPage);
    }

    scan() {
        console.log('Scan captor');
        this.captor.addCaptor(response =>
        {
            this.navCtrl.push(TabsPage);
    });
    }

}

