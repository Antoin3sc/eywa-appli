import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CaptorPage } from './captor';

@NgModule({
  declarations: [
    CaptorPage,
  ],
  imports: [
    IonicPageModule.forChild(CaptorPage),
  ],
})
export class CaptorPageModule {}
