import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";
import { AlertController } from 'ionic-angular';
import { ProfilPage } from '../profil/profil';
import { CaptorProvider } from "../../providers/captor/captor";
import { DetailPlantePage } from "../detail-plante/detail-plante";

@IonicPage()
@Component({
  selector: 'page-captor',
  templateUrl: 'captor.html',
})


export class CaptorPage {
    box: any = [];
    data: any = {};
    tasks: any = [];
    plants: any = [];

    public listPlante: any[];
    public listPlante2: any[];
    public listPlante3: any[];

    plante_vide1: boolean = true;
    plante_vide2: boolean = true;
    plante_vide3: boolean = true;

    nomPlante: any = 'Basilic';

    radioOpen: boolean;
    radioResult: any;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public alertCtrl: AlertController,
                public api: ApiProvider,
                public captor: CaptorProvider) {
    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad CaptorPage');
        this.selectData();
    }

    refresh(){
        this.captor.addCaptor(response => {
            this.selectData();
        });
        }

    selectData() {
        this.api.getBox('admin', 'admin').subscribe(
            (box) => {
                this.box = box;
                console.log('Get All Box From User');
                console.log(box[0].ip);
                this.api.getDataBox('admin', 'admin', box[0].ip).subscribe(
                    (dataBox) => {
                        this.data = {};
                        dataBox = dataBox[0];
                        let moist = null;
                        let temp = null;
                        let light = null;

                        if(dataBox.moist < 20) {
                            moist = 'danger';
                        } else if (dataBox.moist < 25) {
                            moist = 'warning';
                        } else {
                            moist = 'default';
                        }
                        this.data.moist = moist;

                        if(dataBox.temp < 10) {
                            temp = 'danger';
                        } else if (dataBox.temp < 15) {
                            temp = 'warning';
                        } else {
                            temp = 'default';
                        }
                        this.data.temp = temp;

                        if(dataBox.light < 50) {
                            light = 'danger';
                        } else if (dataBox.light < 75) {
                            light = 'warning';
                        } else {
                            light = 'default';
                        }
                        this.data.light = light;


                        console.log(JSON.stringify(this.data));



                    }
                );

                this.api.getTaskBox('admin', 'admin', box.ip).subscribe(
                    (taskBox) => {
                        this.tasks = taskBox;
                    }
                );

                this.api.getPlantsBox('admin', 'admin', box.ipPlant).subscribe(
                    (plants) => {
                        this.plants = plants;
                    }
                )
            }
        );


    }

    addPlant(idBox, idPlant) {
        this.api.setPlantsBox('admin', 'admin', idBox, idPlant)
    }

    profil() {
        this.navCtrl.push(ProfilPage);
    }

    choixPlante1() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Nos plantes');
        alert.addInput({type: 'radio', label: 'Basilic', value: 'Basilic', checked: false});
        alert.addInput({type: 'radio', label: 'Thym', value: 'Thym', checked: false});
        alert.addInput({type: 'radio', label: 'Coriandre', value: 'Coriandre', checked: false});
        alert.addInput({type: 'radio', label: 'Persil', value: 'Persil', checked: false});
        alert.addInput({type: 'radio', label: 'Menthe', value: 'Menthe', checked: false});
        alert.addInput({type: 'radio', label: 'Romarin', value: 'Romarin', checked: false});
        alert.addButton('Fermer');
        alert.addButton({
            text: 'OK',
            handler: data => {
                this.radioOpen = false;
                this.radioResult = data;
                console.log(this.radioResult);
                this.api.getPlanteByTitle(this.radioResult).subscribe(
                    data => {
                        this.listPlante = data;
                        console.log("data" + JSON.stringify(data));
                    },
                    error => {
                        console.log("pas de data" + JSON.stringify(error))
                    }
                );
                this.plante_vide1 = !this.plante_vide1;
            }
        });
        alert.present();
    }

    choixPlante2() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Nos plantes');
        alert.addInput({type: 'radio', label: 'Basilic', value: 'Basilic', checked: false});
        alert.addInput({type: 'radio', label: 'Thym', value: 'Thym', checked: false});
        alert.addInput({type: 'radio', label: 'Coriandre', value: 'Coriandre', checked: false});
        alert.addInput({type: 'radio', label: 'Persil', value: 'Persil', checked: false});
        alert.addInput({type: 'radio', label: 'Menthe', value: 'Menthe', checked: false});
        alert.addInput({type: 'radio', label: 'Romarin', value: 'Romarin', checked: false});
        alert.addButton('Fermer');
        alert.addButton({
            text: 'OK',
            handler: data => {
                this.radioOpen = false;
                this.radioResult = data;
                this.api.getPlanteByTitle(this.radioResult).subscribe(
                    data => {
                        this.listPlante2 = data;
                        console.log("data" + JSON.stringify(data));
                    },
                    error => {
                        console.log("pas de data" + JSON.stringify(error))
                    }
                );
                this.plante_vide2 = !this.plante_vide2;
            }
        });
        alert.present();
    }

    choixPlante3() {
        let alert = this.alertCtrl.create();
        alert.setTitle('Nos plantes');
        alert.addInput({type: 'radio', label: 'Basilic', value: 'Basilic', checked: false});
        alert.addInput({type: 'radio', label: 'Thym', value: 'Thym', checked: false});
        alert.addInput({type: 'radio', label: 'Coriandre', value: 'Coriandre', checked: false});
        alert.addInput({type: 'radio', label: 'Persil', value: 'Persil', checked: false});
        alert.addInput({type: 'radio', label: 'Menthe', value: 'Menthe', checked: false});
        alert.addInput({type: 'radio', label: 'Romarin', value: 'Romarin', checked: false});
        alert.addButton('Fermer');
        alert.addButton({
            text: 'OK',
            handler: data => {
                this.radioOpen = false;
                this.radioResult = data;
                console.log(this.radioResult);
                this.api.getPlanteByTitle(this.radioResult).subscribe(
                    data => {
                        this.listPlante3 = data;
                        console.log("data" + JSON.stringify(data));
                    },
                    error => {
                        console.log("pas de data" + JSON.stringify(error))
                    }
                );
                this.plante_vide3 = !this.plante_vide3;
            }
        });
        alert.present();
    }

    detailPlante(){
        this.navCtrl.push(DetailPlantePage, this.nomPlante)
    }

}