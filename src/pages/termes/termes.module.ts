import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TermesPage } from './termes';

@NgModule({
  declarations: [
    TermesPage,
  ],
  imports: [
    IonicPageModule.forChild(TermesPage),
  ],
})
export class TermesPageModule {}
