import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, App } from 'ionic-angular';
import { NotifPage } from '../notif/notif';
import { TermesPage } from '../termes/termes';
import { AidesPage } from '../aides/aides';
import {WelcomePage} from "../welcome/welcome";

@IonicPage()
@Component({
  selector: 'page-set',
  templateUrl: 'set.html',
})
export class SetPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public appCtrl: App) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SetPage');
  }

    disconnect() {
        this.appCtrl.getRootNav().setRoot(WelcomePage);
    }

termes(){
	this.navCtrl.push(TermesPage);
}

aides(){
	this.navCtrl.push(AidesPage);
}



}
