import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ApiProvider} from "../../providers/api/api";

/**
 * Generated class for the DetailPlantePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detail-plante',
  templateUrl: 'detail-plante.html',
})
export class DetailPlantePage {

    public listPlante: any[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public api: ApiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DetailPlantePage');

    console.log(this.navParams);
      this.api.getPlanteByTitle(this.navParams.data).subscribe(
          data => {
              this.listPlante = data;
              console.log(JSON.stringify(data));
          },
          error => {
              console.log(JSON.stringify(error))
          }
      );
  }

}
