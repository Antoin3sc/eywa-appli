import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailPlantePage } from './detail-plante';

@NgModule({
  declarations: [
    DetailPlantePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailPlantePage),
  ],
})
export class DetailPlantePageModule {}
