import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CaptorsPage } from './captors';

@NgModule({
  declarations: [
    CaptorsPage,
  ],
  imports: [
    IonicPageModule.forChild(CaptorsPage),
  ],
})
export class CaptorsPageModule {}
