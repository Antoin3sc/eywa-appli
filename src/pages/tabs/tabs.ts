import { Component } from '@angular/core';


import { CaptorPage } from '../captor/captor';
import { HomePage } from '../home/home';
import { SearchPage } from '../search/search';

@Component({
    templateUrl: 'tabs.html'
})

export class TabsPage {

    tab1Root = HomePage;
    tab2Root = CaptorPage;
    tab3Root = SearchPage;

    constructor() {

    }



}






