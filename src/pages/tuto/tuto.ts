import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Slides } from 'ionic-angular';
import {AddCaptorPage} from "../add-captor/add-captor";

import { NotificationProvider } from "../../providers/notification/notification";


@IonicPage()
@Component({
  selector: 'page-tuto',
  templateUrl: 'tuto.html',
})
export class TutoPage {

  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

    ionViewDidLoad() {
        console.log('ionViewDidLoad TutoPage');
    }

    skip() {
        this.navCtrl.push(AddCaptorPage);
    }

    slideChanged(){
    if (this.slides.isEnd()){
      this.navCtrl.push(AddCaptorPage);
    }
    }


}
