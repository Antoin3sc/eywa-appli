import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchRecetteDetailPage } from './search-recette-detail';

@NgModule({
  declarations: [
    SearchRecetteDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SearchRecetteDetailPage),
  ],
})
export class SearchRecetteDetailPageModule {}
