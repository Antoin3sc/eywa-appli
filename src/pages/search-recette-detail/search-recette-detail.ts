import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ApiProvider } from "../../providers/api/api";


@IonicPage()
@Component({
  selector: 'page-search-recette-detail',
  templateUrl: 'search-recette-detail.html',
})
export class SearchRecetteDetailPage {

    public listRecette: any[];

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public api: ApiProvider,
              public ngzone:NgZone) {
  }

    ionViewWillEnter() {
      console.log('ionViewDidLoad SearchRecetteDetailPage');

      this.api.getRecettesByTitle(this.navParams.data).subscribe(
          data => {
                  this.listRecette = data;
              console.log(JSON.stringify(data));
          },
          error => {
              console.log(JSON.stringify(error))
          }
      );

    }

    go_back(){
        this.navCtrl.pop();
    }

  }
