import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DescriptionBasilicPage } from './description-basilic';

@NgModule({
  declarations: [
    DescriptionBasilicPage,
  ],
  imports: [
    IonicPageModule.forChild(DescriptionBasilicPage),
  ],
})
export class DescriptionBasilicPageModule {}
