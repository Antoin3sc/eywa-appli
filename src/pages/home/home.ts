import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { DetailRecettePage } from '../detail-recette/detail-recette';
import { ApiProvider } from "../../providers/api/api";
import {ProfilPage} from "../profil/profil";


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

    public listRecette: any[];
    public listTuto: any[];
    public listActu: any[];

    private title = 'pesto';
    private tuto = 'taille';
    private actu = 'site';
    private tuto2 = 'graines';

    liked: boolean = false;
    liked2: boolean = false;
    liked3: boolean = false;
    liked4: boolean = false;

  constructor(public navCtrl: NavController,
              public api: ApiProvider) {
  }

    ionViewWillEnter() {
        console.log('ionViewDidLoad HomePage');

        this.api.getRecettesByTitle(this.title).subscribe(
            data => {
                this.listRecette = data;
                console.log("data"+ JSON.stringify(data));
            },
            error => {
                console.log("pas de data" + JSON.stringify(error))
            }
        );

        this.api.getTutoByTitle(this.tuto).subscribe(
            data => {
                this.listTuto = data;
                console.log("data"+ JSON.stringify(data));
            },
            error => {
                console.log("pas de data" + JSON.stringify(error))
            }
        );

        this.api.getActuByTitle(this.actu).subscribe(
            data => {
                this.listActu = data;
                console.log("data"+ JSON.stringify(data));
            },
            error => {
                console.log("pas de data" + JSON.stringify(error))
            }
        );

        this.api.getTutoByTitle(this.tuto2).subscribe(
            data => {
                this.listTuto = data;
                console.log("data"+ JSON.stringify(data));
            },
            error => {
                console.log("pas de data" + JSON.stringify(error))
            }
        );
    }

    colored(){
        if (this.liked){
            this.liked = false;
        }
        else{
            this.liked = true;
        }
        return this.liked;
    }

    colored2(){
        if (this.liked2){
            this.liked2 = false;
        }
        else{
            this.liked2 = true;
        }
        return this.liked2;
    }

    colored3(){
        if (this.liked3){
            this.liked3 = false;
        }
        else{
            this.liked3 = true;
        }
        return this.liked3;
    }

    colored4(){
        if (this.liked4){
            this.liked4 = false;
        }
        else{
            this.liked4 = true;
        }
        return this.liked4;
    }


    connexion() {
        this.navCtrl.push(DetailRecettePage);
    }

    profil(){
        this.navCtrl.push(ProfilPage);
    }
}

