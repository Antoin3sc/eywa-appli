import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SearchPlantePage } from './search-plante';

@NgModule({
  declarations: [
    SearchPlantePage,
  ],
  imports: [
    IonicPageModule.forChild(SearchPlantePage),
  ],
})
export class SearchPlantePageModule {}
