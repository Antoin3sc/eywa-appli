import { Component } from '@angular/core';
import { Platform, NavController, NavParams } from 'ionic-angular';
import { SearchPage } from '../search/search';


@Component({
  selector: 'page-search-plante',
  templateUrl: 'search-plante.html',
})
export class SearchPlantePage {

  constructor(public navCtrl: NavController, public navParams: NavParams,platform: Platform) {
 
  	let backAction = platform.registerBackButtonAction(() => {
  		console.log("second");
  		this.navCtrl.pop();
  		backAction();
  	  },2)

  	}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SearchPlantePage');
  }


go_back() {
		this.navCtrl.push(SearchPage);
	}

}

