import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailRecettePage } from './detail-recette';

@NgModule({
  declarations: [
    DetailRecettePage,
  ],
  imports: [
    IonicPageModule.forChild(DetailRecettePage),
  ],
})
export class DetailRecettePageModule {}
