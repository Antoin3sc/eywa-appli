import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { BLE } from '@ionic-native/ble';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ProgressBarComponent } from '../components/progress-bar/progress-bar';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { SQLite } from '@ionic-native/sqlite';
import { HttpModule } from '@angular/http';
import { LocalNotifications } from "@ionic-native/local-notifications";
import { BackgroundMode } from '@ionic-native/background-mode';

// Pages
import {ProfilPage} from '../pages/profil/profil';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { SearchPage } from '../pages/search/search';
import { WelcomePage } from '../pages/welcome/welcome';
import { SetPage } from '../pages/set/set';
import { SearchRecettePage } from '../pages/search-recette/search-recette';
import { SearchPlantePage } from '../pages/search-plante/search-plante';
import { CaptorPage } from '../pages/captor/captor';
import { CaptorsPage } from '../pages/captors/captors';
import { AddCaptorPage } from '../pages/add-captor/add-captor';
import { DetailPage } from '../pages/detail/detail';
import { NotifPage } from '../pages/notif/notif';
import { TermesPage } from '../pages/termes/termes';
import { TutoPage } from '../pages/tuto/tuto';
import { DescriptionBasilicPage } from '../pages/description-basilic/description-basilic';
import { AidesPage } from '../pages/aides/aides';
import { SearchRecetteDetailPage } from '../pages/search-recette-detail/search-recette-detail';
import { DetailPlantePage} from "../pages/detail-plante/detail-plante";
import { DetailRecettePage} from "../pages/detail-recette/detail-recette";

//Providers
import { ApiProvider } from "../providers/api/api";
import { SqliteProvider } from '../providers/sqlite/sqlite';
import { CaptorProvider } from '../providers/captor/captor';
import { NotificationProvider } from '../providers/notification/notification';
import { BackgroundmodeProvider } from '../providers/backgroundmode/backgroundmode';

@NgModule({
  declarations: [
    MyApp,
    CaptorPage,
    ProfilPage,
    HomePage,
    TabsPage,
    SearchPage,
    WelcomePage,
    SetPage,
    SearchPlantePage,
    SearchRecettePage,
    AddCaptorPage,
    ProgressBarComponent,
    DetailPage,
    NotifPage,
    TermesPage,
    TutoPage,
    DescriptionBasilicPage,
    AidesPage,
    CaptorsPage,
    SearchRecetteDetailPage,
    DetailPlantePage,
      DetailRecettePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    CaptorPage,
    ProfilPage,
    HomePage,
    TabsPage,
    SearchPage,
    WelcomePage,
    SetPage,
    SearchPlantePage,
    SearchRecettePage,
    AddCaptorPage,
    DetailPage,
    NotifPage,
    TermesPage,
    TutoPage,
    DescriptionBasilicPage,
    AidesPage,
    CaptorsPage,
    SearchRecetteDetailPage,
    DetailPlantePage,
      DetailRecettePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    BLE,
    ScreenOrientation,
    SQLite,
    LocalNotifications,
    BackgroundMode,
    ApiProvider,
    SqliteProvider,
    CaptorProvider,
    NotificationProvider,
    BackgroundmodeProvider,
  ]
})
export class AppModule {}
