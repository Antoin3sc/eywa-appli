import { Injectable } from '@angular/core';
import { LocalNotifications } from "@ionic-native/local-notifications";
import { SQLite, SQLiteObject } from '@ionic-native/sqlite';
import { Platform } from "ionic-angular";

import { SqliteProvider } from "../sqlite/sqlite";



@Injectable()
export class NotificationProvider {

  constructor(
      private notification: LocalNotifications,
      private sqlite: SQLite,
      private dbb: SqliteProvider,
      private platform: Platform
  ) {
    console.log('Service Notification');
  }

    pushNotification() {
        this.notification.schedule({
            title:'Attention ! Votre plante à besoin de vous !',
            text: 'Le basilic à besoin d\'eau',
            at: new Date(new Date().getTime() + 100),
            led: 'FF0000',
            icon: 'assets/icon/favicon.ico'
        });
    }

    testNotification() {
        this.notification.schedule({
            title:'Connexion!',
            text: '',
            at: new Date(new Date().getTime() + 100),
            led: 'FF0000',
            icon: 'assets/icon/favicon.ico'
        });
    }
}
