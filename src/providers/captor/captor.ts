import { Injectable } from '@angular/core';
import { BLE } from '@ionic-native/ble';
import { Platform } from 'ionic-angular';
import { ApiProvider } from "../api/api";
import {isArray} from "ionic-angular/es2015/util/util";


const DATABASE_FILE_NAME: string = 'data.db';


@Injectable()
export class CaptorProvider {

    peripheral: any = {};
    characW: string;
    characR: string;
    service: string;
    data: any = [];

    constructor(public plt: Platform,
                private ble : BLE,
                private api: ApiProvider)
    {
    console.log('Service Captor');

        if (this.plt.is('ios')) {
            this.service = '00001204-0000-1000-8000-00805F9B34FB';
            this.characW = '00001A00-0000-1000-8000-00805F9B34FB';
            this.characR = '00001A01-0000-1000-8000-00805F9B34FB';
        } else if (this.plt.is('android')){
            this.service = '1204';
            this.characW = '1A00';
            this.characR = '1A01';
        }
    }

    addCaptor(callback = null) {
        console.log('Scanning for Bluetooth LE Devices');
        this.ble.enable(); // Work Only Android
        this.ble.scan([], 25).subscribe(
            device =>
            {
                this.onDeviceDiscovered(device, callback);
                },
            error => {
                console.log(JSON.stringify(error));
            }
        );

        return new Promise(resolve => true);
    }

    onDeviceDiscovered(device, callback) {
        if(device.name == "Flower care"){
            // console.log('Discovered ' + JSON.stringify(device, null, 2));
            this.checkDataBase(device.id);
            this.connect(device, callback);
        }
    }

    private checkDataBase(ipDevice) {
        console.log('check api');

        this.api.getBox('admin', 'admin')
        .subscribe(
         (response) => {
                if(response.length == 0){
                    console.log('add New Box');
                    this.api.setNewBox('admin', 'admin', ipDevice)
                        .subscribe((data) => console.log('data box : ' + data), (error) => console.log('error : ' + JSON.stringify(error)));
                }
            },
            (error) => console.log(JSON.stringify(error))
        );
    }

    connect(device, callback) {
        this.ble.connect(device.id).subscribe(
            peripheral => this.onConnected(peripheral, callback),
            peripheral => this.onDeviceDisconnected()
        );
    }


    onConnected(peripheral, callback) {
        this.peripheral = peripheral;
        let edit = new Uint8Array([0xA0, 0x1F]).buffer;

        this.ble.write(peripheral.id, this.service, this.characW, edit).then(
                () => console.log('enabled get data'),
                (error) => console.log(JSON.stringify(error))
        );
        this.ble.read(peripheral.id, this.service, this.characR).then(
            buffer => {
                let data = new Uint8Array(buffer);

                //Temperature
                let temp = (data[1] * 256 + data[0]) / 10;
                //Humidité
                let moist = data[7];
                //Lumière
                let light = data[4] * 256 + data[3];
                //Conductivité
                let conduc = data[9] * 256 + data[8];

                this.api.insertDataBox('admin', 'admin', peripheral.id, temp, moist, light, conduc)
                    .subscribe((success) => {
                            if (callback != null) {
                                callback(null, callback);
                            }
                        },
                        (error) => console.log('error : ' + JSON.stringify(error)));
            }
        );
    }

    onDeviceDisconnected() {
        console.log('periphrique deconnecté');
    }

    // Disconnect peripheral when leaving the page
    ionViewWillLeave() {
        console.log('ionViewWillLeave disconnecting Bluetooth');
        this.ble.disconnect(this.peripheral.id).then(
            () => console.log('Disconnected ' + JSON.stringify(this.peripheral)),
            () => console.log('ERROR disconnecting ' + JSON.stringify(this.peripheral))
        )
    }


}
