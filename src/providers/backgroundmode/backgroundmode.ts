import { Injectable } from '@angular/core';
import { BackgroundMode } from "@ionic-native/background-mode";
import { NotificationProvider } from "../notification/notification";
import { Observable } from 'rxjs/Rx';

@Injectable()
export class BackgroundmodeProvider {

  delayNum: number = 1000;

    constructor(
    private bgMode:BackgroundMode,
    private notif: NotificationProvider)
    {
        console.log('Hello BackgroundmodeProvider Provider');
    }
    //
    initBackgroundMode() {
        this.notif.testNotification();
        this.bgMode.enable();
        this.bgMode.on("enable").subscribe(() => console.log('test'), (error) => console.log(JSON.stringify(error)));

        this.bgMode.on("activate").subscribe(() => this.initNotifBackground(), (error) => console.log(JSON.stringify(error)));
    }

    initNotifBackground() {
        Observable.interval(1000 * 60 * 60).subscribe(x => {




            this.notif.pushNotification();
        });
    }
    }
