import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import { SQLite,SQLiteObject } from "@ionic-native/sqlite";

const DATABASE_FILE_NAME: string = 'data.db';

@Injectable()
export class SqliteProvider {
    private db: SQLiteObject;

    constructor(public http: Http, private sqlite: SQLite) {
        console.log('Service SQLite');
    }

    public dbbSqlite(action, params) {
        this.sqlite.create({
            name: DATABASE_FILE_NAME,
            location: 'default'
        })
        .then((db: SQLiteObject) => {
            console.log('bdd créée');
            this.db = db;
            switch(action){
                case 'init' :
                    console.log('init Bdd SqLite');
                    this.dbInit();
                    break;
                case 'drop' :
                    console.log('drop Bdd SqLite');
                    this.dbDrop();
                    break;
                case 'resetUser' :
                    console.log('reset User Bdd SqLite');
                    this.resetUser();
                    break;
                case 'insertUser' :
                    console.log('new User Bdd SqLite');
                    this.insertUser(params[0], params[1]);
                    break;
            }
        });

    }

    private dbInit() {
        this.db.executeSql('CREATE TABLE IF NOT EXISTS `User` ( `idUsers` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `nom` TEXT, `prenom` TEXT, `age` INTEGER, `login` TEXT NOT NULL, `mdp` TEXT NOT NULL, `mail` TEXT )', {})
        .then(
            () => {
                console.log('Table User Créée');
                this.db.executeSql('CREATE TABLE IF NOT EXISTS `Parameter` ( `idParams` INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, `tuto` INTEGER, `notif` INTEGER, `lang` TEXT)', {})
                    .then(
                        () => {
                            console.log('Table Parameter Créée');
                        });
            },
        (error) => {
            console.log(JSON.stringify(error));
        })
    }

    private dbDrop() {
        this.db.executeSql('DROP TABLE `User`', {})
        .then(
        () => {
            console.log('Table User Supprimé');
            this.db.executeSql('DROP TABLE `Parameter`', {})
                .then(
                () => {
                    console.log('Table Parameter Supprimé');
                    this.db.executeSql('DROP TABLE `Box`', {})
                        .then(
                        () => {
                            console.log('Table Box Supprimé');
                            this.db.executeSql('DROP TABLE `Data`', {})
                                .then(
                                () => {
                                    console.log('Table Data Supprimé');
                                },
                                (error) => {
                                    console.log(JSON.stringify(error));
                                });
                            },
                        (error) => {
                            console.log(JSON.stringify(error));
                        });
                    },
                (error) => {
                    console.log(JSON.stringify(error));
                });
            },
        (error) => {
            console.log(JSON.stringify(error));
        })
    }

    private resetUser() {
        this.db.executeSql('DELETE FROM `User`', {})
        .then(
            () => console.log('DB User Truncate'),
            () => console.log('error')
        )
    }

    private insertUser(login, mdp) {
        this.db.executeSql("INSERT INTO `User`(login,mdp) VALUES ('"+ login +"', '"+ mdp +"')", {})
            .then(
                () => console.log('added'),
                () => console.log('error')
            );
    }
}