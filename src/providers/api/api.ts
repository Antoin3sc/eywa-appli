import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

//connexion asynchrone
import 'rxjs/add/operator/map';

/*
  Generated class for the TestsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApiProvider {
    private url:string;

    constructor(public http: Http) {
        console.log('Lancement de l api');
        this.url =  'http://api.eywabox.fr';
    }

    getAuthWhereLogin(login, mdp)
    {
        return this.http.get(this.url + "?action=auth&login=" + login + "&mdp="+ mdp)
            .map(res => res.json());
    }

    getUserInformations(id)
    {
        return this.http.get(this.url + "?action=getUserInformations&id=" + id)
            .map(res => res.json());
    }


    getRecettesByTitle(search)
    {
        return this.http.get(this.url + "?action=recettesByTitle&title=" + search)
            .map(res => res.json());
    }

    getTutoByTitle(search)
    {
        return this.http.get(this.url + "?action=tutoByTitle&title=" + search)
            .map(res => res.json());
    }

    getActuByTitle(search)
    {
        return this.http.get(this.url + "?action=actuByTitle&title=" + search)
            .map(res => res.json());
    }

    getPlanteByTitle(search)
    {
        return this.http.get(this.url + "?action=planteByName&title=" + search)
            .map(res => res.json());
    }

    setNewBox(login, password, ipBox)
    {
        return this.http.get(this.url + "?action=setNewBox&login=" + login + "&mdp=" + password + "&ipBox=" + ipBox)
            .map(res => res.json());
    }

    insertDataBox(login, password, ipBox, temp, moist, light, conduc)
    {
        return this.http.get(this.url + "?action=insertDataBox&login=" + login + "&mdp=" + password + "&ipBox=" + ipBox + "&temp=" + temp + "&moist=" + moist + "&light=" + light + "&conduc=" + conduc);
    }

    getBox(login, password)
    {
        return this.http.get(this.url + "?action=getBox&login=" + login + "&mdp=" + password )
            .map(res => res.json());
    }

    getDataBox(login, password, ipBox)
    {
        return this.http.get(this.url + "?action=getDataBox&login=" + login + "&mdp=" + password + "&ipBox=" + ipBox )
            .map(res => res.json());
    }

    getTaskBox(login, password, ipBox)
    {
        return this.http.get(this.url + "?action=getTaskBox&login=" + login + "&mdp=" + password + "&ipBox=" + ipBox )
            .map(res => res.json());
    }

    getPlantsBox(login, password, ipPlants)
    {
        return this.http.get(this.url + "?action=getPlantsBox&login=" + login + "&mdp=" + password + "&ipPlants=" + ipPlants )
            .map(res => res.json());
    }

    setPlantsBox(login, password, idBox, ipPlants)
    {
        return this.http.get(this.url + "?action=getPlantsBox&login=" + login + "&mdp=" + password + "&ipPlants=" + ipPlants + "&idBox=" + idBox)
            .map(res => res.json());
    }

}





